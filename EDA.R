library(dplyr)
library(ggplot2)

sales_dat <- read.csv('data/sku_sales.csv',header = T)

summary(sales_dat)
# severe missing values but somehow could be categoried

glimpse(sales_dat)

sales_dat %>% group_by(item_sku_id,dc_id) %>% summarise(cnt=n()) %>% summary()

# dc vs date
sales_dat %>% group_by(dc_id,date) %>% summarise(qty=sum(quantity)) %>%
ggplot(.) + geom_line(aes(as.Date(date), qty, group=dc_id, color=dc_id))

sales_dat %>% group_by(dc_id,date) %>% summarise(qty=sum(log(quantity+1))) %>%
ggplot(.) + geom_line(aes(as.Date(date), qty, group=dc_id, color=dc_id)) +
  facet_wrap(~dc_id, scales = "free_y")


# item vs date for fun only: NOT SO USEFUL
sales_dat %>% group_by(item_sku_id,date) %>% summarise(qty=sum(quantity)) %>%
ggplot(.) + geom_line(aes(as.Date(date), qty, group=item_sku_id, color=item_sku_id))


# item, dc vs date
sales_dat %>% distinct(item_sku_id, dc_id) %>% sample_n(9) %>% inner_join(sales_dat) %>% 
  ggplot(.) + geom_line(aes(as.Date(date), quantity)) + 
  facet_wrap(~dc_id + item_sku_id,scales = "free_y")

# somehow have to merge products(hate!), MAYBE modelling(somewhat favored) way to
# split products(need further thinking).

## imputation for discount seems not necessary because it needs quantity to calucate and 
## quantity is the one to be predicted.

# original price
sales_dat %>% filter(item_sku_id %in% sample(unique(item_sku_id),size = 9)) %>%
  ggplot() + geom_bar(aes(as.Date(date), fill=original_price)) +
  facet_wrap(~item_sku_id,scales = 'free')

## original price could be used, however since there is no original price in test either.
## maybe first try is to not use original price.

# ================================= sku infos ===============================

# dc vs. higher level
info_dat <- read.csv('data/sku_info.csv')
summary(info_dat)

## first category
sales_dat %>% left_join(info_dat) %>% group_by(date,item_first_cate_cd, dc_id) %>% 
  summarise(qty=sum(quantity)) %>% 
  ggplot(.) + geom_line(aes(as.Date(date), qty)) + 
  facet_wrap(~dc_id + item_first_cate_cd,scales = "free_y")

## second category
sales_dat %>% distinct(item_sku_id, dc_id) %>% sample_n(9) %>% 
  inner_join(sales_dat) %>% inner_join(info_dat) %>%
  group_by(date,item_second_cate_cd, dc_id) %>% 
  summarise(qty=sum(quantity)) %>% 
  ggplot(.) + geom_line(aes(as.Date(date), qty)) +
  facet_wrap(~ dc_id + item_second_cate_cd,scales = "free_y")

## third category
sales_dat %>% distinct(item_sku_id, dc_id) %>% sample_n(9) %>% 
  inner_join(sales_dat) %>% inner_join(info_dat) %>%
  group_by(date,item_third_cate_cd, dc_id) %>% 
  summarise(qty=sum(quantity)) %>% 
  ggplot(.) + geom_line(aes(as.Date(date), qty)) +
  facet_wrap(~ dc_id + item_third_cate_cd,scales = "free_y")

## sku
sales_dat %>% distinct(item_sku_id, dc_id) %>% sample_n(6) %>% inner_join(sales_dat) %>% 
  ggplot(.) + geom_line(aes(as.Date(date), quantity)) + 
  facet_wrap(~dc_id + item_sku_id,scales = "free_y")


## brand
sales_dat %>% distinct(item_sku_id, dc_id) %>% sample_n(9) %>% 
  inner_join(sales_dat) %>% inner_join(info_dat) %>%
  group_by(date,brand_code, dc_id) %>% 
  summarise(qty=sum(quantity)) %>% 
  ggplot(.) + geom_line(aes(as.Date(date), qty)) +
  facet_wrap(~ dc_id + brand_code,scales = "free_y")


## portions of skus on every day
sales_dat %>% distinct(item_sku_id) %>% sample_n(2) %>%
  inner_join(sales_dat %>% filter(quantity > 0) %>% group_by(date, dc_id) %>%
               summarise(tot = sum(quantity)) %>% right_join(sales_dat) %>%
               mutate(ratio = quantity/tot)) %>% 
  ggplot(.) +
  geom_line(aes(as.Date(date), ratio)) + facet_wrap(~dc_id + item_sku_id)


## number of sku categories that sell every day w.r.t dc
sales_dat %>% filter(quantity > 0) %>% distinct(date, dc_id,item_sku_id) %>%
  group_by(date,dc_id) %>% summarise(cnt = n()) %>%
  ggplot(.) + geom_line(aes(as.Date(date),cnt)) + facet_wrap(~dc_id, scales = 'free_y')

sales_dat %>% filter(quantity > 0) %>% distinct(date, dc_id,item_sku_id) %>%
  group_by(date,dc_id) %>% summarise(cnt = n()) %>%
  ggplot(.) + geom_line(aes(as.Date(date),cnt, col = dc_id))

sales_dat %>% filter(quantity > 0) %>% distinct(date, dc_id,item_sku_id) %>%
  group_by(date,dc_id) %>% summarise(cnt = n()) %>% 
  filter(dc_id == 1, as.Date(date) > '2017/1/1') %>%
  data.frame(.)

## so the number of sku categories selled in each dc is increasing rapidly, which is probably why
## the total sales increasing as well. Interstingly, the number of sku categories selled in each dc
## drops rapidly around january(2017-1-27) with sd about 7 days each year, 
## and it turns out to be spring festival. 

## TIPS 1: divide time into months, days, year, when predicting.
## something worth to try for another project: the time series with average curve or 
## (quantity, num_category) as response variables


## after looked at the sales trend w.r.t dc vs date and #category, dc vs. date, it seems that 
## generally constant trend is more common for skus. The increasing trend on dc vs. date level is 
## due to the increasing number of skus selled, which is likely to be caused by more and more skus
## that have been added into selling list.

## to check this, first to see sku trend in general is constant or not
sales_dat %>% distinct(item_sku_id) %>% sample_n(9) %>% inner_join(sales_dat) %>%
  group_by(item_sku_id, date) %>% summarise(quantity = sum(quantity)) %>%
  ggplot(.) + geom_line(aes(as.Date(date), quantity)) + 
  facet_wrap(~item_sku_id,scales = "free_y")

## then to see average sales trend of dc vs. date is constant or not
sales_dat %>% filter(quantity > 0) %>% distinct(date, dc_id,item_sku_id) %>%
  group_by(dc_id, date) %>% summarise(cnt = n()) %>% ungroup() %>%
  left_join(sales_dat %>% group_by(dc_id,date) %>% summarise(qty=sum(quantity)) %>% ungroup()) %>%
  mutate(average_sales = qty/cnt) %>%
  ggplot(.) + geom_line(aes(as.Date(date), average_sales)) + 
  geom_smooth(aes(as.Date(date), average_sales)) +
  facet_wrap(~dc_id, scales = "free_y")

## note: dividing by #categories would be helpful with trend, not so sure about the rest.
## another way is to see the rate of sku in each dc is constant over time


## how many series for sku, dc that could be used to build model
sales_dat %>% filter(quantity > 0) %>% group_by(item_sku_id,dc_id) %>% summarise(cnt = n()) %>% 
  filter(cnt > 90) %>% group_by(dc_id) %>% summarise(cnt = n()/1000)

## overall about 80% of products could be used to build a model locally,
## which leaves about 200 skus to be predicted, if left with no choice for modelling other ways. 
## I don't like this way because that will need a lot of "accurate" features to extract based on 
## other infos which will be very painful.

# ================================= promotion ===============================
prom_dat <- read.csv('data/sku_prom.csv',header = T)
prom_dat$date <- gsub('/', '-',prom_dat$date)
summary(prom_dat)

# promotion effect in general (sku, dc vs. date)
sales_dat %>% left_join(prom_dat) %>% group_by(dc_id, date) %>%
  summarise(quantity = sum(quantity), prom = all(is.na(promotion_type))) %>% ungroup() %>%
  ggplot(.) + geom_line(aes(as.Date(date), quantity)) +
  geom_point(aes(as.Date(date), quantity, col = prom)) +
  facet_wrap(~dc_id)
## so basically at least one sku have promotions every day.

## promotion type effect
sales_dat %>% left_join(prom_dat) %>% group_by(dc_id, date) %>%
  summarise(quantity_orig = sum(quantity), 
            quantity_prom = sum(quantity[!is.na(promotion_type)]),
            quantity_non = sum(quantity[is.na(promotion_type)])) %>% 
  ungroup() %>%
  ggplot(.) + geom_line(aes(as.Date(date), quantity_orig, col='Original')) +
  geom_line(aes(as.Date(date), quantity_prom, col='Promotion')) +
  geom_line(aes(as.Date(date), quantity_non, col='None Promotion')) +
  facet_wrap(~dc_id, scales = 'free_y')
## most sku are selled when they have promotion

### promotion for each kind and total sales
summary(as.factor(prom_dat$promotion_type))
sales_dat %>% left_join(prom_dat) %>% group_by(dc_id, date) %>% 
  summarise(quantity_orig = sum(quantity), 
            quantity_non = sum(quantity[is.na(promotion_type)]),
            quantity_1 = sum(quantity[promotion_type == 1],na.rm = T),
            quantity_4 = sum(quantity[promotion_type==4],na.rm = T),
            quantity_6 = sum(quantity[promotion_type==6], na.rm = T),
            quantity_10 = sum(quantity[promotion_type==10], na.rm = T)) %>% 
  ungroup() %>%
  ggplot(.) + geom_line(aes(as.Date(date), quantity_orig, col='Original')) +
  geom_line(aes(as.Date(date), quantity_non, col='Non Promotion')) +
  geom_line(aes(as.Date(date), quantity_1, col='Promotion 1')) +
  geom_line(aes(as.Date(date), quantity_4, col='Promotion 4')) +
  geom_line(aes(as.Date(date), quantity_6, col='Promotion 6')) +
  geom_line(aes(as.Date(date), quantity_10, col='Promotion 10')) +
  facet_wrap(~dc_id, scales = 'free_y')

# log may be useful for data transformation!!
summary(as.factor(prom_dat$promotion_type))
sales_dat %>% left_join(prom_dat) %>% group_by(dc_id, date) %>% 
  mutate(quantity = log(quantity+1)) %>%
  summarise(quantity_orig = sum(quantity), 
            quantity_non = sum(quantity[is.na(promotion_type)]),
            quantity_1 = sum(quantity[promotion_type == 1],na.rm = T),
            quantity_4 = sum(quantity[promotion_type==4],na.rm = T),
            quantity_6 = sum(quantity[promotion_type==6], na.rm = T),
            quantity_10 = sum(quantity[promotion_type==10], na.rm = T)) %>% 
  ungroup() %>%
  ggplot(.) + geom_line(aes(as.Date(date), quantity_orig, col='Original')) +
  geom_line(aes(as.Date(date), quantity_non, col='Non Promotion')) +
  geom_line(aes(as.Date(date), quantity_1, col='Promotion 1')) +
  geom_line(aes(as.Date(date), quantity_4, col='Promotion 4')) +
  geom_line(aes(as.Date(date), quantity_6, col='Promotion 6')) +
  geom_line(aes(as.Date(date), quantity_10, col='Promotion 10')) +
  facet_wrap(~dc_id, scales = 'free_y')


### promotion for each kind
sales_dat %>% left_join(prom_dat) %>% group_by(dc_id, date, promotion_type) %>% 
  summarise(quantity = sum(log(quantity+1))) %>% 
  ungroup() %>%
  ggplot(.) + 
  geom_line(aes(as.Date(date), quantity, group = promotion_type,col=as.factor(promotion_type))) +
  facet_wrap(~dc_id)

sales_dat %>% left_join(prom_dat) %>% group_by(dc_id, date, promotion_type) %>% 
  summarise(quantity = sum(log(quantity+1))) %>% 
  ungroup() %>%
  ggplot(.) + 
  geom_line(aes(as.Date(date), quantity, col=promotion_type)) +
  facet_wrap(~dc_id + promotion_type,ncol = 5)

## promotion affect sales in the most significant way: 10 ~= 1 > 6 > 4 > 10

## re look at the sku vs. date in each dc on the log scale

sales_dat %>% distinct(item_sku_id, dc_id) %>% sample_n(6) %>% inner_join(sales_dat) %>% 
  mutate(quantity = log(quantity+1)) %>%
  ggplot(.) + geom_line(aes(as.Date(date), quantity)) + 
  facet_wrap(~dc_id + item_sku_id,scales = "free")


sales_dat %>% distinct(item_sku_id, dc_id) %>% sample_n(9) %>% 
  inner_join(sales_dat) %>% inner_join(info_dat) %>%
  mutate(quantity = log(quantity+1)) %>%
  group_by(date,item_third_cate_cd, dc_id) %>% 
  summarise(qty=sum(quantity)) %>% 
  ggplot(.) + geom_line(aes(as.Date(date), qty)) +
  facet_wrap(~ dc_id + item_third_cate_cd,scales = "free_y")


# predict sku and then split into dc??
sales_dat %>% distinct(item_sku_id) %>% sample_n(6) %>% inner_join(sales_dat) %>%
  group_by(item_sku_id,date) %>% summarise(qty=sum(quantity)) %>%
  ggplot(.) + geom_line(aes(as.Date(date), qty, group=item_sku_id, color=as.factor(item_sku_id))) +
  facet_wrap(~item_sku_id)


sales_dat %>% distinct(item_sku_id) %>% sample_n(6) %>% inner_join(sales_dat) %>%
  group_by(item_sku_id,date) %>% summarise(qty=sum(log(quantity+1))) %>%
  ggplot(.) + geom_line(aes(as.Date(date), qty, group=item_sku_id)) +
  facet_wrap(~item_sku_id)

sales_dat %>% distinct(item_sku_id) %>% sample_n(6) %>% inner_join(sales_dat) %>% 
  group_by(item_sku_id, date) %>%
  mutate(ratio = quantity/sum(quantity), ratio_log = log(quantity+1)/sum(log(quantity+1))) %>%
  ungroup() %>%
  ggplot(.) + geom_line(aes(as.Date(date), ratio, group=dc_id, color=as.factor(dc_id))) +
  facet_wrap(~item_sku_id, scales = 'free')


sales_dat %>% distinct(item_sku_id) %>% sample_n(6) %>% inner_join(sales_dat) %>% 
  group_by(item_sku_id, date) %>%
  mutate(ratio = quantity/sum(quantity), ratio_log = log(quantity+1)/sum(log(quantity+1))) %>%
  ungroup() %>%
  ggplot(.) + geom_line(aes(as.Date(date), ratio_log, group=dc_id, color=as.factor(dc_id))) + 
  facet_wrap(~item_sku_id, scales = 'free')

sales_dat %>% distinct(item_sku_id) %>% sample_n(6) %>% inner_join(sales_dat) %>% 
  group_by(item_sku_id, date) %>%
  mutate(ratio = quantity/sum(quantity), ratio_log = log(quantity+1)/sum(log(quantity+1))) %>%
  ungroup() %>%
  ggplot(.) + geom_line(aes(as.Date(date), ratio_log, color="ratio_log")) +
  facet_wrap(~item_sku_id + dc_id, scales = 'free')

sales_dat %>% distinct(item_sku_id) %>% sample_n(6) %>% inner_join(sales_dat) %>% 
  group_by(item_sku_id, date) %>%
  mutate(ratio = quantity/sum(quantity), ratio_log = log(quantity+1)/sum(log(quantity+1))) %>%
  ungroup() %>%
  ggplot(.) + geom_line(aes(as.Date(date), ratio, color="ratio")) +
  geom_smooth(aes(as.Date(date), ratio)) + 
  facet_wrap(~item_sku_id + dc_id)
## seems to be a direction to forecast sales of different sku in general and then split into different dc_id


# ============================= attribute data ================================
attr_dat <- read.csv('data/sku_attr.csv',header = T)
summary(attr_dat)


tmp <- attr_dat %>% distinct(item_sku_id, attr_cd) %>% 
  group_by(item_sku_id) %>% summarise(cnt = n()) %>% ungroup() %>% arrange(cnt)
tapply(rep(1,length(tmp$cnt)),tmp$cnt,sum)

# how to use attributes of a item??
attr_dat %>% distinct(item_sku_id, attr_cd) %>% 
  group_by(attr_cd) %>% summarise(cnt = n()) %>%  ungroup() %>% arrange(cnt) %>% data.frame()
## not so sure, at least it can't be used as factors, common categories are few.


# difference between effect of value alone and effect of value combined


# ============================ test data ======================================
test <- read.csv('data/sku_prom_testing_2018Jan.csv',header = T)

glimpse(test)
# it seems that discount should not be used in this scenario since there is data available for predicting.

# how about original price
sales_dat %>% distinct(item_sku_id, original_price) %>% group_by(item_sku_id) %>% tally()








